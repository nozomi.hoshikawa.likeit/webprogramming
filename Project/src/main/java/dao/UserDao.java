package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			//登録可能
			if (!rs.next()) {
				return null;
			}
			String login_id = rs.getString("login_id");
			String name = rs.getString("name");
			String birth_date = rs.getString("birth_date");
			String password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");

			return new User(login_id, name, birth_date, password, create_date, update_date);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			/* データベースへ接続、データベースからコネクションを作る */
			conn = DBManager.getConnection();

			/* SELECTを準備 */
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			/*SELECTを実行、結果表を取得*/
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			/* ログイン失敗時の処理 */
			if (!rs.next()) {
				return null;
			}

			/* ログイン成功時の処理 */
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findbyId(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			//SELECTを実行し結果表を取得
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int ID = rs.getInt("id");
			String login_id = rs.getString("login_id");
			String name = rs.getString("name");
			String birth_date = rs.getString("birth_date");
			String password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");

			return new User(ID, login_id, name, birth_date, password, create_date, update_date);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user";

			//SELECTを実行し結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> SearchUser(String loginId, String userName, Date birthdayStartdate, Date birthdayEnddate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id like ? AND name like ? AND birth_date BETWEEN ? AND ? ";

			// INSERTを実行
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, "%" + userName + "%");
			ps.setDate(3, birthdayStartdate);
			ps.setDate(4, birthdayEnddate);
			ResultSet rs = ps.executeQuery();

			userList = ResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return userList;
	}

	public User InsertUser(User user) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String password = passwordEncrypt(user.getPassword());

			// INSERT文を準備
			String sql = "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date) VALUES (?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, user.getLoginId());
			pStmt.setString(2, user.getName());
			pStmt.setString(3, user.getBirthDate());
			pStmt.setString(4, password);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return user;
	}

	public User updateUser(User user) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String password = passwordEncrypt(user.getPassword());

			// UPDATE文を準備
			String sql = "UPDATE user SET name=?, birth_date=?, password=? WHERE id =?";

			// INSERTを実行(? の数だけsetしないといけない)
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getName());
			ps.setString(2, user.getBirthDate());
			ps.setString(3, password);
			ps.setInt(4, user.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	public User deleteUser(User user) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "DELETE FROM user WHERE id =?";

			// INSERTを実行
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	//パスワードの暗号化メソッド
	public String passwordEncrypt(String source) {
		String result = null;
		Connection conn = null;
		try {
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";

			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			result = DatatypeConverter.printHexBinary(bytes);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	//resultsetを受け取ってUserインスタンスを作って返すメソッド
	public List<User> ResultSet(ResultSet rs) {
		List<User> userList = new ArrayList<User>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;

	}

}
