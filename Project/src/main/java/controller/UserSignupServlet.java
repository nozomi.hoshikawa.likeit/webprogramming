package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserSignup
 */
@WebServlet("/UserSignupServlet")
public class UserSignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSignupServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");

		User User = new User(loginId, name, birthDate, password, passwordConfirm);
		UserDao userDao = new UserDao();

		/*一つでも未入力の項目があった場合のエラー*/
		if (loginId == "" || name == "" || birthDate == "" || password == "" || passwordConfirm == "") {
			request.setAttribute("errMsg", "未入力の項目があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/*passwordがpassword(確認)と異なる場合のエラー*/
		if (!password.equals(passwordConfirm)) {
			request.setAttribute("errMsg", "入力されたパスワードが異なっています");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}

		User user = userDao.findByLoginInfo(loginId);//ログインIDを持つUser型のやつ持ってきて
		/*既に登録されているログインIDを入力した場合のエラー*/
		if (user != null) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;

		} else {
			User newUser = userDao.InsertUser(User);
			response.sendRedirect("UserListServlet");
		}

	}

}
