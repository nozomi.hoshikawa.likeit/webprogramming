package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//もしセッションがあったら、UserListServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser != null) {
			response.sendRedirect("UserListServlet");
			return;
		}

		//もしセッションがなかったらlogin.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*リクエストパラメータの文字コード指定*/
		request.setCharacterEncoding("UTF-8");

		/*リクエストパラメータの入力項目を取得*/
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		/*リクエストパラメータの入力項目を引数に渡してDaoのメソッドを実行
		 （ログイン成功時はUser型のインスタンス、失敗時はNullが返ってくる）*/
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(loginId, password);

		/*テーブルに該当のデータ無し（ログイン失敗時）*/
		if (user == null) {
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");//リクエストスコープにエラーメッセージをセット
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return; //login.jspにフォワード
		}
		/*テーブルに該当のデータ有り（ログイン成功時）*/
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);//セッションにユーザの情報をセット
		response.sendRedirect("UserListServlet");//UserListServletにリダイレクト
	}

}
