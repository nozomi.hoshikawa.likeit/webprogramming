package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		/*ユーザ情報を取得*/
		UserDao userDao = new UserDao();
		User user = userDao.findbyId(id);

		/*リクエストスコープにユーザ一覧情報をセット*/
		request.setAttribute("user", user);

		/*userUpdate.jspへフォワード*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.findbyId(id);

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");

		/*password以外に未入力の項目があった場合のエラー*/
		if (name == "" || birthDate == "") {
			request.setAttribute("errMsg", "未入力の項目があります");
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/*passwordがpassword(確認)と異なる場合のエラー*/
		if (!password.equals(passwordConfirm)) {
			request.setAttribute("errMsg", "入力されたパスワードが異なっています");
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/*インスタンス上で更新*/
		user.setName(name);
		user.setBirthDate(birthDate);

		/*passwordに入力があった場合その値で更新 (入力が無かった場合はDBに入っている値で更新=変化なし)*/
		if (password != "") {
			user.setPassword(password);
		}

		/*DB上で更新*/
		userDao.updateUser(user);

		response.sendRedirect("UserListServlet");
	}

}
