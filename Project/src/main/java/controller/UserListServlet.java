package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		/*ユーザ一覧情報を取得*/
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		/*userListをstreamに変換　loginIdがadmin以外のuserを抽出　userListに代入*/
		userList = userList.stream()
				.filter(u -> !u.getLoginId().equals("admin"))
				.collect(Collectors.toList());

		/*リクエストスコープにユーザ一覧情報をセット*/
		request.setAttribute("userList", userList);

		/*userList.jspへフォワード*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//jspから入力された文字をもらう
		String loginId = request.getParameter("loginId");
		String userName = request.getParameter("userName");
		String birthdayStartdateStr = request.getParameter("birthdayStartdate");
		String birthdayEnddateStr = request.getParameter("birthdayEnddate");

		if (loginId == "") {
			loginId = "%%";
		}

		if (birthdayStartdateStr == "") {
			birthdayStartdateStr = "1000-01-01";
		}
		if (birthdayEnddateStr == "") {
			birthdayEnddateStr = "9999-12-31";
		}

		Date birthdayStartdate = Date.valueOf(birthdayStartdateStr);
		Date birthdayEnddate = Date.valueOf(birthdayEnddateStr);

		/*ユーザ情報を取得*/
		UserDao userDao = new UserDao();
		List<User> userList = userDao.SearchUser(loginId, userName, birthdayStartdate, birthdayEnddate);

		/*userListをstreamに変換　loginIdがadmin以外のuserを抽出　userListに代入*/
		userList = userList.stream()
				.filter(u -> !u.getLoginId().equals("admin"))
				.collect(Collectors.toList());

		/*リクエストスコープにユーザ一覧情報をセット*/
		request.setAttribute("userList", userList);

		/*userList.jspへフォワード*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);
	}

}
