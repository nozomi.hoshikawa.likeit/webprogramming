<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>ユーザ新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />
	
</head>
<body>
	<header>
		<nav class="navbar-expand">
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-text navbar-brand" href="LoginServlet">ユーザ管理システム </a>
					</div>

					<ul class="navbar-nav">
						<li class="navbar-text">${userInfo.name}さん</li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</nav>
		</nav>
	</header>

	<h1 class="mt-5 mb-5 text-center">ユーザ新規登録</h1>
	
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<div class="container">

		<div class="body p-5">
			<form action="UserSignupServlet" method="post">

				<div class="form-group row">
					<label for="loginId" class="col-md-3 col-form-label">ログインID</label>
					<div class="col-lg-8 col-md-7 mr-auto">
						<input type="text" class="form-control" name="loginId" />
					</div>
				</div>

				<div class="form-group row">
					<label for="password" class="col-md-3 col-form-label">パスワード</label>
					<div class="col-lg-8 col-md-7 mr-auto">
						<input type="password" class="form-control" name="password" />
					</div>
				</div>

				<div class="form-group row">
					<label for="passwordConfirm" class="col-md-3 col-form-label">パスワード(確認)</label>
					<div class="col-lg-8 col-md-7 mr-auto">
						<input type="password" class="form-control" name="passwordConfirm" />
					</div>
				</div>

				<div class="form-group row">
					<label for="name" class="col-md-3 col-form-label">ユーザ名</label>
					<div class="col-lg-8 col-md-7 mr-auto ">
						<input type="text" class="form-control" name="name" />
					</div>
				</div>

				<div class="form-group row">
					<label for="birthDate" class="col-md-3 col-form-label">生年月日</label>
					<div class="col-lg-8 col-md-7 mr-auto">
						<input type="date" class="form-control" name="birthDate" />
					</div>
				</div>
				<br>

				<div class="text-center">
					<input type="submit" class="btn btn-primary col-lg-2 col-md-2"
						value="登録" />
				</div>

				<div class="text-left mt-5">
					<a class="return" href="UserListServlet">戻る</a>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
