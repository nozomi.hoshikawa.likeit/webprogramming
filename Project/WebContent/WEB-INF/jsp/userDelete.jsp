<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>ユーザ削除確認</title>
<link
	rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous"/>
<link href="userDelete.css" rel="stylesheet" type="text/css" />

</head>
<body>
  
	<header>
		<nav class="navbar-expand">
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-text navbar-brand" href="LoginServlet">ユーザ管理システム </a>
					</div>

					<ul class="navbar-nav">
						<li class="navbar-text">${userInfo.name}さん</li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</nav>
		</nav>
	</header>
  
    <h1 class="mt-5 mb-5 text-center">ユーザ削除確認</h1>
    
    <div class="container">
    	<form action="UserDeleteServlet?id=${user.id}" method="post">
      	<div class="row">
      	 <div class="col">
          ログインID : ${user.loginId}<br />
          を本当に削除してよろしいでしょうか。
        </div>
      </div>
    </div>
    <br />
    <div class="check">
      <a class="btn btn-cancel" href="UserListServlet?id=${user.id}">キャンセル</a>
      <input type="submit" value="OK" />
    </div>
   </form>
  </body>
</html>
