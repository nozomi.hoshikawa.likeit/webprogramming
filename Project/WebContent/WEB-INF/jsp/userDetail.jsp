<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />
</head>
<body>
	<header>
		<nav class="navbar-expand">
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-text navbar-brand" href="LoginServlet">ユーザ管理システム </a>
					</div>

					<ul class="navbar-nav">
						<li class="navbar-text">${userInfo.name}さん</li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</nav>
		</nav>
	</header>

	<h1 class="mt-5 mb-5 text-center">ユーザ情報詳細参照</h1>

	<div class="container">

		<div class="card m-4">
			<div class="table-responsive">
				<table class="table mb-0">
					<thead class="thead-light">
						<tr>
							<th class="text-nowrap">ログインID</th>
							<th class="text-nowrap">ユーザ名</th>
							<th>生年月日</th>
							<th>登録日時</th>
							<th>更新日時</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<td>${user.createDate}</td>
							<td>${user.updateDate}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
			<div class="text-left mt-5">
			<a class="return" href="UserListServlet">戻る</a>
			</div>
	</div>
</body>
</html>
