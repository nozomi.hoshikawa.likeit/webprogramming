<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />
</head>
<body>

	<header>
		<nav class="navbar-expand">
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-text navbar-brand" href="LoginServlet">ユーザ管理システム
						</a>
					</div>

					<ul class="navbar-nav">
						<li class="navbar-text">${userInfo.name}さん</li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</nav>
		</nav>
	</header>

	<h1 class="mt-5 mb-5 text-center">ユーザー覧</h1>

	<div class="container">

		<div class="text-right">
			<a href="UserSignupServlet">新規登録</a>
		</div>

		<div class="card m-4">
			<div class="card-header">検索条件</div>

			<div class="body p-5">
				<form class="form-horizontal" action="UserListServlet" method="post">

					<div class="form-group row">
						<label for="loginId" class="col-lg-2 col-md-3 col-form-label">ログインID</label>
						<div class="col-lg-8 col-md-7">
							<input type="text" class="form-control" name="loginId">
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-lg-2 col-md-3 col-form-label">ユーザ名</label>
						<div class="col-lg-8 col-md-7">
							<input type="text" class="form-control" name="userName">
						</div>
					</div>

					<div class="form-group row">
						<label for="birthday" class="col-lg-2 col-md-3 col-form-label">生年月日</label>
						<div class="col-lg-8 col-md-7 pr-0 row">
							<div class="col pr-0 pr-lg-3">
								<input type="date" class="form-control" name="birthdayStartdate">
							</div>
							<span class="col-lg-1">~</span>
							<div class="col pr-0">
								<input type="date" class="form-control" name="birthdayEnddate">
							</div>
						</div>
					</div>

					<br>
					<div class="text-right">
						<input type="submit" class="btn btn-primary col-lg-2 col-md-2"
							value="検索">
					</div>
				</form>
			</div>
		</div>

		<div class="card m-4">
			<div class="table-responsive">
				<table class="table table-hover mb-0">
					<thead class="thead-light">
						<tr>
							<th class="text-nowrap">ログインID</th>
							<th class="text-nowrap">ユーザ名</th>
							<th>生年月日</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${userList}">
							<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td class="text-nowrap">${user.birthDate}</td>
							
							<td class="text-nowrap">
								<a class="btn btn-primary"href="UserDetailServlet?id=${user.id}">詳細</a> 
								<c:if test="${userInfo.loginId =='admin' || userInfo.loginId == user.loginId}">
									<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a></c:if>
								<c:if test="${userInfo.loginId =='admin'}">
									<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a></c:if>
							</td>
							
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</body>
</html>
