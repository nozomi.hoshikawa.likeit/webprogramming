<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />
</head>
<body>

	<header>
		<nav class="navbar-expand">
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-text navbar-brand" href="LoginServlet">ユーザ管理システム
						</a>
					</div>

					<ul class="navbar-nav">
						<li class="navbar-text">${userInfo.name}さん</li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</nav>
		</nav>
	</header>

	<h1 class="mt-5 mb-5 text-center">ユーザ情報更新</h1>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>


	<div class="text-center">
		<form action="UserUpdateServlet?id=${user.id}" method="post">
			<div class="container">
				<div class="row">
					<div class="col"></div>

					<div class="col-10">

						<div class="form-group row">
							<label for="loginId" class="col-sm-5 col-form-label">ログインID</label>
							<div class="col-sm-5">${user.loginId}</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-sm-5 col-form-label">パスワード</label>
							<div class="col-sm-5">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group row">
							<label for="passwordConfirm" class="col-sm-5 col-form-label">パスワード(確認)</label>
							<div class="col-sm-5">
								<input type="password" class="form-control"
									name="passwordConfirm">
							</div>
						</div>

						<div class="form-group row">
							<label for="name" class="col-sm-5 col-form-label">ユーザ名</label>
							<div class="col-sm-5">
								<input type="text" value="${user.name}" class="form-control"
									name="name">
							</div>
						</div>

						<div class="form-group row">
							<label for="birthDate" class="col-sm-5 col-form-label">生年月日</label>
							<div class="col-sm-5">
								<input type="date" value="${user.birthDate}"
									class="form-control" name="birthDate">
							</div>
						</div>

						<input type="submit" value="更新" class="m-4" />

						<div class="text-left">
							<a class="return"
								href="file:///Users/Nozomi/Documents/WebProgramming/Mock/userList.html">戻る</a>
						</div>

					</div>
				</div>
			</div>
		</form>
	</div>

</body>
</html>
