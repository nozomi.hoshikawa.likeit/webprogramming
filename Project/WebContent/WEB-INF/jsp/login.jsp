<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />

</head>
<body>
	<header>
		<nav class="navbar-expand">
			<nav class="navbar navbar-dark bg-dark">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-text navbar-brand" href="LoginServlet">ユーザ管理システム </a>
					</div>
				</div>
			</nav>
		</nav>
	</header>

	<h1 class="mt-5 mb-5 text-center">ログイン画面</h1>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<div class="text-center">
		<form action="LoginServlet" method="post">
			<div class="container">
				<div class="row">
					<div class="col"></div>

					<div class="col-10">
						
							<div class="form-group row">
								<label for="loginId" class="col-sm-5 col-form-label">ログインID</label>
								<div class="col-sm-5">
									<input type="text" name="loginId" class="form-control"
										id="loginId" />
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-sm-5 col-form-label">パスワード</label>
								<div class="col-sm-5">
									<input type="password" name="password" class="form-control"
										id="password" />
								</div>
							</div>
							<br /> <br /> <input type="submit" value="ログイン" />
					
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>
